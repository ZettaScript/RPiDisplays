#!/usr/bin/env python3
"""
Copyright 2017-2018 Pascal Engélibert
"""

# Get the bit b of n
def getbit(n, b):
	return n&(1<<b) != 0

# Reverse a byte (0b11010001 => 0b10001011)
def reverse(b):
	b = (b&0xf0)>>4|(b&0x0f)<<4
	b = (b&0xcc)>>2|(b&0x33)<<2
	b = (b&0xaa)>>1|(b&0x55)<<1
	return b

# Create a bitmap
def bitmapInit():
	return bytearray(1024)

# Invert bitmap 1/0
def bitmapInvert(data):
	ret = bytearray()
	for c in data:
		ret.append(c^255)
	return ret

# Rotate a bitmap by 180°
def bitmapRotate(data):
	ret = bytearray()
	i = 63
	while i >= 0:
		s = i*16
		k = 15
		while k >= 0:
			ret.append(reverse(data[s+k]))
			k -= 1
		i -= 1
	return ret

# Convert bytes or bytearray to bits array
def bytearrayBits(data):
	ret = []
	for c in data:
		for i in range(8):
			ret.append(getbit(c, 7-i))
	return ret

# Convert bits array to bytearray
def bitsBytearray(data):
	ret = bytearray()
	B = 0
	o = 7
	for b in data:
		B |= b<<o
		o -= 1
		if o < 0:
			ret.append(B)
			B = 0
			o = 7
	return ret

# Add text to a bitmap
def bitmapText(bitmap, text, font, x, y, inv=False):
	bitmap = bytearrayBits(bitmap)
	n = 0
	for c in text:
		if c == "\n":
			x = 0
			y += font[0]
		elif c == "\r":
			x = 0
		else:
			try:
				lines = font[1][c]
			except KeyError:
				lines = font[1]["<?>"]
			if x+lines[0] > 128:
				x = 0
				y += font[0]
			cy = y
			for line in lines[1]:
				cx = x
				i = lines[0]
				while i >= 0:
					bitmap[cy*128+cx] = getbit(line, i)^inv
					cx += 1
					i -= 1
				cy += 1
			x += lines[0]
		n += 1
	return bitsBytearray(bitmap)

# draw a horizontal line
def bitmapHLine(bitmap, v, x1, y1, x2):
	bitmap = bytearrayBits(bitmap)
	while x1 <= x2:
		bitmap[y1*128+x1] = v
		x1 += 1
	return bitsBytearray(bitmap)

# Draw a vertical line
def bitmapVLine(bitmap, v, x1, y1, y2):
	bitmap = bytearrayBits(bitmap)
	while y1 <= y2:
		bitmap[y1*128+x1] = v
		y1 += 1
	return bitsBytearray(bitmap)
