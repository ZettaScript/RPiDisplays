#!/usr/bin/env python3
"""
Copyright 2017-2018 Pascal Engélibert
Raspberry Pi LCD12864 SPI library
See datasheet:
http://www.digole.com/images/file/Digole_12864_LCD.pdf
"""

from time import sleep, time
from threading import Thread, RLock
from utils import *

FREQ = 220000# normal frequency (Hz)
FREQ_SLEEP = 100# frequency in sleep mode (Hz)
PULSE = 1/FREQ# pulse duration (s)
PULSE_SLEEP = 1/FREQ_SLEEP# pulse duration in sleep mode (s)
SLEEP_DELAY = 0.01# delay to enter in sleep mode (saves CPU) (s)
INST1_DELAY = 17.28*PULSE# instruction duration at custom frequency
INST2_DELAY = 3456.4*PULSE# some instructions are slower

SID = 20# data (MOSI) pin
CLK = 21# clock pin
pins = [SID, CLK]
pixbuf = [[0 for y in range(16)] for x in range(32)]

lock = RLock()

class SendBuf(Thread):
	def __init__(self):
		Thread.__init__(self)
		self.databuf = []# [RS, BYTE]
		self.work = True
	
	def run(self):
		bytebuf = []
		delay = 0
		spdelay = 0# instruction specific delay
		sleeping = False
		outgpio = gpio.output
		LOW = gpio.LOW
		HIGH = gpio.HIGH
		while self.work:
			if delay > 0:
				outgpio(SID, LOW)
				if time() > delay:
					delay = 0
			if delay == 0 and len(bytebuf) > 0:
				if bytebuf.pop(0):
					outgpio(SID, HIGH)
				else:
					outgpio(SID, LOW)
				if len(bytebuf) == 0 and spdelay > 0:
					delay = time() + spdelay
			if len(bytebuf) == 0 and len(self.databuf) > 0:
				with lock:
					data = self.databuf.pop(0)
				d = data[1]
				if d != -1:
					bytebuf = [True,True,True,True,True,False,data[0],False,d&0x80,d&0x40,d&0x20,d&0x10,False,False,False,False,d&0x08,d&0x04,d&0x02,d&0x01,False,False,False,False]
				spdelay = data[2]
				sleeping = False
			outgpio(CLK, HIGH)
			sleep(PULSE)
			if sleeping:
				sleep(PULSE_SLEEP)
			outgpio(CLK, LOW)
			sleep(PULSE)
			if sleeping:
				sleep(PULSE_SLEEP)
			elif len(bytebuf) == 0:
				if time() > delay:
					sleeping = True
	
	# Return false if the buffer is empty
	def busy(self):
		return self.databuf != []

sendbuf = SendBuf()

# Init GPIO and start the com thread
def initDisplay():
	gpio.setwarnings(False)
	gpio.setmode(gpio.BCM)
	gpio.setup(pins, gpio.OUT, initial=gpio.LOW)
	sendbuf.start()

# Transform real x,y to LCD x,y (the LCD coords are very uggly)
# cx=x ; cy=y ; bx=bit offset
def coord(x, y):
	cx = int(x/16)+8*(y>31)
	bx = 15-x%16
	cy = y%32
	return cx, cy, bx

# the 3rd parameter in databuf is the time for the LCD to execute the instruction
# At 240kHz: 72µs=0.000072 ; At 10kHz: 0.001728

# Clear display
def displayClear():
	with lock:
		sendbuf.databuf.append([False, 0x01, INST2_DELAY])

standby = displayClear

# Set counter to 0 and cursor to origin
def returnHome():
	with lock:
		sendbuf.databuf.append([False, 0x02, INST1_DELAY])

# d:display  c:cursor  b:character blink
def displayControl(d, c, b):
	with lock:
		sendbuf.databuf.append([False, 0x08+d*4+c*2+b, INST1_DELAY])

# b=1:8bits b=0:4bits ; e=1:extended e=0:basic
def functionSet(b, e):
	with lock:
		sendbuf.databuf.append([False, 0x22+b*16+e*4, INST1_DELAY])

# Write byte to internal RAM
def writeRAM(data):
	with lock:
		sendbuf.databuf.append([True, data, INST1_DELAY])

# Set DDRAM address (<64) (RE=0)
def setDDRAM(address):
	with lock:
		sendbuf.databuf.append([False, 0x80+address, INST1_DELAY])

# x<16 y<64 (RE=1)
def setGDRAM(x, y):
	with lock:
		sendbuf.databuf.append([False, 0x80+y, INST1_DELAY])
		sendbuf.databuf.append([False, 0x80+x, INST1_DELAY])

# Set CGRAM address (<64) (RE=0)
def setCGRAM(address):
	with lock:
		sendbuf.databuf.append([False, 0x40+address, INST1_DELAY])

# Wait s seconds between two paquets
def wait(s):
	with lock:
		sendbuf.databuf.append([False, -1, s])

# Prepare the screen for drawing
def initDrawing():
	functionSet(1,0)
	displayControl(1,0,0)
	displayClear()
	functionSet(1,1)

# Draw a pixel
def setPixel(x, y, v):
	cx, cy, bx = coord(x, y)
	if getbit(pixbuf[cy][cx], bx) != v:
		if v:
			pixbuf[cy][cx] |= 1 << bx
		else:
			pixbuf[cy][cx] &= ~(1 << bx)
		bytes = pixbuf[cy][cx]
		setGDRAM(cx, cy)
		writeRAM(bytes >> 8)
		writeRAM(bytes & 0xff)

# Draw a line
def setLine(y, v):
	o = v*255
	for i in range(0,y):
		setGDRAM(0, i)
	for x in range(16):
		writeRAM(o)
	#for x in range(32,48):
	#	writeRAM(o)
	setGDRAM(0,0)

# Set scroll 0 pixels at v (RE=1)
def fillScreen(v=0, scroll=0):
	o = v*255
	for y in range(33):
		setGDRAM(0, y+scroll)
		for x in range(16):
			writeRAM(o)
		for x in range(32,48):
			writeRAM(o)
	setGDRAM(0,0)
	for y in range(32):
		for x in range(16):
			pixbuf[y][x] = v

# Like fillScreen, using setPixel
def clearPixels():
	for cy in range(32):
		for cx in range(16):
			if pixbuf[cy][cx] != 0:
				pixbuf[cy][cx] = 0
				setGDRAM(cx, cy)
				writeRAM(0)
				writeRAM(0)

# Draw text
# fonts can be found in fonts.py
def setText(text, font, x, y, inv=False):
	n = 0
	for c in text:
		if c == "\n":
			x = 0
			y += font[0]
		elif c == "\r":
			x = 0
		else:
			try:
				lines = font[1][c]
			except KeyError:
				lines = font[1]["<?>"]
			if x+lines[0] > 128:
				x = 0
				y += font[0]
			cy = y
			for line in lines[1]:
				cx = x
				i = lines[0]
				while i >= 0:
					setPixel(cx, cy, getbit(line, i)^inv)
					cx += 1
					i -= 1
				cy += 1
			x += lines[0]
		n += 1
	setGDRAM(0,0)

# Draw a bitmap
def setBitmap(data, scroll=0):
	i = 0
	while i < 32:
		setGDRAM(0, i+scroll)
		s = i*16
		k = 0
		while k < 16:
			writeRAM(data[s+k])
			k += 1
		k = 0
		while k < 16:
			writeRAM(data[512+s+k])
			k += 1
		i += 1
	setGDRAM(0,0)

# Set scroll address (RE=1)
setScroll = setCGRAM

# Stop com thread
def stop():
	sendbuf.work = False
	sendbuf.join()
	gpio.cleanup()

busy = sendbuf.busy
