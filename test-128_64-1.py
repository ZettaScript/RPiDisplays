#!/usr/bin/env python3
"""
Copyright 2017-2018 Pascal Engélibert
"""

import lcd12864 as lcd
import fonts
from time import sleep, localtime

# INIT DISPLAY
lcd.initDisplay()# init communication thread
sleep(0.1)
lcd.initDrawing()# graphic mode
sleep(0.1)
lcd.fillScreen()# clear screen

while lcd.busy():
	sleep(0.1)

lcd.setText("Hello world!", fonts.STD_FONT, 8, 32)

try:
	while True:
		t = localtime()
		lcd.setText(str(t.tm_year)+"-"+str(t.tm_mon)+"-"+str(t.tm_mday)+"\n"+str(t.tm_hour)+":"+str(t.tm_min)+":"+str(t.tm_sec), fonts.STD_FONT, 0, 1)
		while lcd.busy():
			sleep(0.1)
except KeyboardInterrupt:
	lcd.stop()
	exit()

lcd.stop()
